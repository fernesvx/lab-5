using LabWeek5.Models;
using ReactiveUI;

namespace LabWeek5.ViewModels;

public class ResultWindowViewModel : ViewModelBase
{
    public Pesquisador Pesquisador { get; set; }

    public string NumeroDeUsuariosEncontrados
    {
        get => _numeroDeUsuariosEncontrados; 
        set => this.RaiseAndSetIfChanged(ref _numeroDeUsuariosEncontrados, value);
    }
    private string _numeroDeUsuariosEncontrados;
    
    public ResultWindowViewModel(Pesquisador pesquisador)
    {
        Pesquisador = pesquisador;
        _numeroDeUsuariosEncontrados = Pesquisador.ResultadoDaPesquisa!.Count+" Usuários econtrados";
    }
}