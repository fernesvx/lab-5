﻿using System;
using System.Reactive;
using LabWeek5.Models;
using LabWeek5.Views;
using ReactiveUI;

namespace LabWeek5.ViewModels;

public class MainWindowViewModel : ViewModelBase
{
    public string? Nome { get; set; }
    public string? Sobrenome { get; set; }
    public string? Email { get; set; }

    public bool CheckBoxAscedenteMarcado { get; set; }

    public bool CheckBoxDescendenteMarcado { get; set; }

    public string? Notificacao
    {
        get => _notificacao;
        set => this.RaiseAndSetIfChanged(ref _notificacao, value);
    }
    public ResultWindow? ResultWindow;
    private Notificadora _notificadora = new();
    
    public string? EmailPesquisado
    {
        get => _emailPesquisado;
        set => this.RaiseAndSetIfChanged(ref _emailPesquisado, value);
    }

    private string? _emailPesquisado;
    public Cadastrador Cadastrador {get; set;}
    public ReactiveCommand<Unit, Unit> ConfirmarCommand { get; set; }
    public ReactiveCommand<Unit, Unit> PesquisarCommand { get; set; }
    public event EventHandler<string>? CheckBoxChecked;
    public Pesquisador Pesquisador;
    public string? NumeroDeCadastrados 
    {
        get => _numeroDeCadastrados;
        set => this.RaiseAndSetIfChanged(ref _numeroDeCadastrados, value); 
    }

    private string? _numeroDeCadastrados;
    private string? _notificacao;

    public MainWindowViewModel()
    {
        ConfirmarCommand = ReactiveCommand.Create(Confirmar);
        PesquisarCommand = ReactiveCommand.Create(Pesquisar);
        Cadastrador = new Cadastrador();
        NumeroDeCadastrados = "Número de usuários registrados: "+ Cadastrador.Usuarios.Count;
        Pesquisador = new Pesquisador(Cadastrador);
        ResultWindow = new ResultWindow();
        AdicionarObservador(new OrdemDePesquisaObservador(this));
    }

    public void Confirmar()
    {
        if (Validador.GetInstance().Validar(this))
        {
            AdicionarObservador(new ConfirmarObservador(this));
            NotificarObservador();
            RemoverObservador(new ConfirmarObservador(this));
            Cadastrador.Cadastrar(Nome, Sobrenome, Email);
            NumeroDeCadastrados = "Número de usuários registrados: " + Cadastrador.Usuarios.Count;
        }
    }

    public void Pesquisar()
    {
        AdicionarObservador(new PesquisarObservador(this));
        NotificarObservador();
        RemoverObservador(new PesquisarObservador(this));
        Pesquisador.Pesquisar(_emailPesquisado, CheckBoxAscedenteMarcado, CheckBoxDescendenteMarcado);
        ResultWindowViewModel resultWindowViewModel = new ResultWindowViewModel(Pesquisador);
        ResultWindow resultWindow = new ResultWindow();
        resultWindow.DataContext = resultWindowViewModel;
        resultWindow.Closed += (_, _) => { ResultWindow = null; };
        resultWindow.Show();
    }

    public void AdicionarObservador(IObservador observador)
    {
        _notificadora.AdicionarAssinante(observador);
    }

    public void RemoverObservador(IObservador observador)
    {
        _notificadora.RemoverAssinante(observador);
    }

    public void NotificarObservador()
    {
        _notificadora.Notificar();
    }

    public void OnCheckBoxChecked(string checkBoxName)
    {
        CheckBoxChecked?.Invoke(this, checkBoxName);
        if (checkBoxName == "CheckBoxAscendente")
        {
            CheckBoxAscedenteMarcado = true;
            CheckBoxDescendenteMarcado = false;
        }
        else if (checkBoxName == "CheckBoxDescendente")
        {
            CheckBoxDescendenteMarcado = true;
            CheckBoxAscedenteMarcado = false;
        }
        else
        {
            CheckBoxAscedenteMarcado = false;
            CheckBoxDescendenteMarcado = false;
        }
        AdicionarObservador(new OrdemDePesquisaObservador(this));
        NotificarObservador();
        RemoverObservador(new OrdemDePesquisaObservador(this));
    }
    
}