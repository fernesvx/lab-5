using Avalonia.Controls;
using Avalonia.Interactivity;
using LabWeek5.ViewModels;

namespace LabWeek5.Views
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void CheckBox_Checked(object? sender, RoutedEventArgs routedEventArgs)
        {
            var checkBox = (CheckBox)sender!;

            if (checkBox.IsChecked == true)
            {
                if (checkBox.Content!.ToString() == "Ascendente")
                {
                    var checkBoxDecrescente = this.Find<CheckBox>("CheckBoxDescendente");
                    if (checkBoxDecrescente != null)
                    {
                        checkBoxDecrescente.IsChecked = false;
                    }
                }
                else if (checkBox.Content.ToString() == "Descendente")
                {
                    var checkBoxAscendente = this.Find<CheckBox>("CheckBoxAscendente");
                    if (checkBoxAscendente != null)
                    {
                        checkBoxAscendente.IsChecked = false;
                    }
                }
                (DataContext as MainWindowViewModel)?.OnCheckBoxChecked(checkBox.Name!);
            }
            else
            {
                (DataContext as MainWindowViewModel)?.OnCheckBoxChecked("");
            }
        }
    }
}