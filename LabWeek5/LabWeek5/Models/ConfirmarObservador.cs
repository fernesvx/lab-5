using LabWeek5.ViewModels;

namespace LabWeek5.Models;

public class ConfirmarObservador : IObservador
{
    private MainWindowViewModel _subject;

    public ConfirmarObservador(MainWindowViewModel viewModel)
    {
        _subject = viewModel;
    }
    public void Atualizar()
    {
        _subject.Notificacao =  "Usuario "+ _subject.Nome + ", adicionado";
    }
}