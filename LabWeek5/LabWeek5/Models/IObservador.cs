namespace LabWeek5.Models;

public interface IObservador
{
    void Atualizar();
}