using LabWeek5.ViewModels;

namespace LabWeek5.Models;

public class PesquisarObservador : IObservador
{
    private MainWindowViewModel _subject;

    public PesquisarObservador(MainWindowViewModel sujeito)
    {
        _subject = sujeito;
    }

    public void Atualizar()
    {
        _subject.Notificacao =  "Pesquisado pelo email: " + _subject.EmailPesquisado;
    }
}