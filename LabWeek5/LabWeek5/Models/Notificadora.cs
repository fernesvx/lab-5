using System.Collections.Generic;

namespace LabWeek5.Models;

public class Notificadora
{
    private List<IObservador> _observers = new();
    public void AdicionarAssinante(IObservador observador)
    {
        _observers.Add(observador);
    }

    public void RemoverAssinante(IObservador observador)
    {
        _observers.Remove(observador);
    }

    public void Notificar()
    {
        foreach (var observador in _observers)
        {
            observador.Atualizar();
        }
    }
}