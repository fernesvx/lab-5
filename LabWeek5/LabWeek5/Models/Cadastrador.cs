using System.Collections.ObjectModel;

namespace LabWeek5.Models;

public class Cadastrador
{
    public ObservableCollection<Usuario> Usuarios { get; set; }
    public Cadastrador()
    {
        Usuarios = new ObservableCollection<Usuario>();
    }

    public void Cadastrar(string? nome, string? sobrenome, string? email)
    {
        Usuarios.Add(new Usuario(nome, sobrenome, email));
    }
}