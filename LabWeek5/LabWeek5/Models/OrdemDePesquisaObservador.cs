
using LabWeek5.ViewModels;

namespace LabWeek5.Models;

public class OrdemDePesquisaObservador : IObservador
{
    private MainWindowViewModel _subject;

    public OrdemDePesquisaObservador(MainWindowViewModel viewModel)
    {
        _subject = viewModel;
    }

    public void Atualizar()
    {
        if (_subject.CheckBoxAscedenteMarcado)
        {
            _subject.Notificacao = "Pesquisa Ascendente marcada";
        }
        else if(_subject.CheckBoxDescendenteMarcado)
        {
            _subject.Notificacao = "Pesquisa Descendente marcada";
        }
        else
        {
            _subject.Notificacao = "";
        }
    }
}