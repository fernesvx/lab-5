using System.Collections.ObjectModel;
using System.Linq;

namespace LabWeek5.Models;

public class Pesquisador(Cadastrador cadastrador)
{
    public Cadastrador Cadastrador = cadastrador;
    public ObservableCollection<Usuario>? ResultadoDaPesquisa { get; set; }

    public void Pesquisar(string? email, bool ascendente, bool descendente)
    {
        if (ascendente)
        {
            ResultadoDaPesquisa = new ObservableCollection<Usuario>(Cadastrador.Usuarios.Where(usuario =>
                usuario.Email!.Contains(email!)).OrderBy(usuario => usuario.Email)
                .ThenBy(usuario => usuario.Nome).ToList());
        }

        if (descendente)
        {
            ResultadoDaPesquisa = new ObservableCollection<Usuario>(Cadastrador.Usuarios.Where(usuario =>
                usuario.Email!.Contains(email!)).OrderByDescending(usuario => usuario.Email)
                .ThenBy(usuario => usuario.Nome).ToList());
        }
        else
        {
            ResultadoDaPesquisa = new ObservableCollection<Usuario>(Cadastrador.Usuarios.Where(usuario =>
                usuario.Email!.Contains(email!)).ToList());
        }
    }
}