namespace LabWeek5.Models;

public class Usuario
{
    public string? Nome { get; set; }
    public string? Sobrenome { get; set; }
    public string? Email { get; set; }

    public Usuario(string? nome, string? sobrenome, string? email)
    {
        Nome = nome;
        Sobrenome = sobrenome;
        Email = email;
    }
}