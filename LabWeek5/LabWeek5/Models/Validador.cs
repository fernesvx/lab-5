using System.Text.RegularExpressions;
using LabWeek5.ViewModels;

namespace LabWeek5.Models
{
    public class Validador
    {
        private static Validador _instance;
        
        private Validador()
        {
        }
        
        public static Validador GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Validador();
            }
            return _instance;
        }

        public bool Validar(MainWindowViewModel viewModel)
        {
            if (!Regex.IsMatch(viewModel.Nome, "^[a-zA-Z]{3,}$") || string.IsNullOrEmpty(viewModel.Nome))
            {
                viewModel.Notificacao = "Nome inválido, o nome deve conter pelo menos 3 letras";
                return false;
            }
            
            if (!Regex.IsMatch(viewModel.Sobrenome, "^[a-zA-Z]{3,}$") || string.IsNullOrEmpty(viewModel.Sobrenome))
            {
                viewModel.Notificacao = "Sobrenome inválido, o sobrenome deve conter pelo menos 3 letras";
                return false;
            }

            if (!Regex.IsMatch(viewModel.Email, @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$") 
                || string.IsNullOrEmpty(viewModel.Email))
            {
                viewModel.Notificacao = "Email inválido, o email deve estar no formato example@example.com";
                return false;
            }

            return true;
        }
        
    }
}